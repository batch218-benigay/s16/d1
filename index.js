//console.log("Hello World!");

/* [SECTION] ASSIGNMENT OPERATOR
	Basic assignment operator (=)
	The assignment operator assigns the value of the right hand operand to a variable*/

	let assignmentNumber = 8;
	console.log("The value of the Assignment Number variable: " + assignmentNumber);
	console.log("-----------------------------");	

// [SECTION] ARITHMETIC OPERATIONS

	let x = 200;
	let y = 18;
	console.log("x: " + x);
	console.log("y: " + y);
	console.log("-----------------------------");

	// ADDITION
	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// SUBTRACTION
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	// MULTIPLICATION
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	// DIVISION
	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// MODULO - to get the remainder
	let modulo = x % y;
	console.log("Result of modulo operator: " + modulo);

// CONTINUATION OF ASSIGNMENT OPEATOR

	/* ADDITION ASSIGNMENT OPERATOR
		Syntax: assignmentNumber = assignmentNumber + value; (long method)

		For the example below, the current assignmentNumber is 8
		*/

		/* ADDITION
				assignmentNumber = assignmentNumber +2;
				console.log(assignmentNumber); 

			SUBTRACTION
				assignmentNumber = assignmentNumber - 3;
				console.log(assignmentNumber);//assignmentNumber value: 7
			
			MULTIPLICATION
				assignmentNumber = assignmentNumber * 3;
				console.log(assignmentNumber);//assignmentNumber value: 7
			
			DIVISION
				assignmentNumber = assignmentNumber / 3;
				console.log(assignmentNumber);//assignmentNumber value: 7
				*/

		// SHORT METHOD

		//Addition
		assignmentNumber += 2;
		console.log("The result of Addition Assignment Operator: " +assignmentNumber); //assignmentNumber value is 10

		assignmentNumber -= 3;
		console.log("The result of Subtraction Assignment Operator: " +assignmentNumber);

		// Multiplication
		assignmentNumber *= 2;
		console.log("The result of Multiplication Assignment Operator: " +assignmentNumber);

		// Division
		assignmentNumber /= 2;
		console.log("The result of Division Assignment Operator: " +assignmentNumber);

/* [SECTION] PEMDAS (Order of Operations)
		Multiple Operators and Parenthesis */

		let mdas = 1 + 2 - 3 * 4 / 5;
		console.log("Result of mdas operation is: " + mdas);
		/* The operations were done in the following order:

		 3 * 4 = 12 | 1 + 2 - 12 / 5
		 12 / 5 = 2.4 | 1 + 2 - 2.4
		 1 + 2 = 3	| 3 - 2.4
		 =0.6
		 */

		 let pemdas = (1+(2-3)) * (4/5);
		 console.log("Result of pemdas operation: " + pemdas);

	/*HEIRARCHY
		Combinations of multiple arithmetic operatins will follow the pemdas rule

		1. Parenthesis
		2. Exponent
		3. Multiplication or Division
		4. Addition or Subtraction

		Note: will also follow left to right rule. */


/* [SECTION] INCREMENT AND DECREMENT
	Increment - increasing
	Decrement - decreasing

	Operators that add or subtract values by 1 and reassign the value of the variable where the increment (++) / decrement (--) was applied */

		let z = 1;

	// pre-increment
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);// value: 2
	console.log("Result of pre-increment of z: " + z);// value: 2

	//post-increment
	increment = z++;
	console.log("Result of post-increment: " + increment); // value: 2
	console.log("Result of post-increment of z: " + z); //value: 3

/*	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment of z: " + z); */


	//Decrement

	//pre-drecrement
	let decrement = --z;
	console.log("Result of pre-decrement is " + decrement); //value: 2
	console.log ("Result of pre-decrement of z: " + z); //value: 2

	//post-decrement
	decrement = z--;
	console.log("Result of pre-decrement: " + decrement); //value: 2
	console.log("Result of pre-decrement of z: " + z); //value: 1


/* [SECTION] TYPE COERCION
	is the automatic conversion of values from one data type to another */

	//combination of numbe and string data type will result to string
	let numA = 10; // number
	let numB = "12"; //string
	let coercion = numA + numB;
	console.log(coercion); // result is 1012
	console.log(typeof coercion);

	//number and number data type will result to number
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numX = 10; //number
	let numY = true //boolean (values are true (1) and false (0))
	coercion = numX + numY;
	console.log(coercion);
	console.log(typeof coercion); //result is number

	let num1 = 1;
	let num2 = false;
	coercion = num1 + num2;
	console.log(coercion);
	console.log(typeof coercion);

/* [SECTION] COMPARISON OPERATORS
	are used to evaluate and compare the left and right operands.
	-After evaluation, it returns a boolean value. */

	// EQUALITY OPERATOR ( == equal to) - compares the value but not the data type
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1') //true
	console.log(0 == false); // true (false boolean value is zero)

	let juan = "juan";
	console.log('juan' == 'juan'); //true
	console.log('juan' == 'Juan'); //false (case sensitive)
	console.log(juan == 'juan'); //true

	//INEQUALITY OPERATOR ( != also read as not equal
	console.log("-----------------------------");
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1') //false
	console.log(0 != false); //false

	console.log('juan' != 'juan'); //false
	console.log('juan' != 'Juan'); //true
	console.log(juan != 'juan'); //false

/* [SECTION] RELATIONAL OPERATORS
	Some comparison operators to check whether one value is greater or less than the other value
	-It will return a true or false value */

	console.log("-----------------------------");
	let a = 50;
	let b = 65;

	// Greater than ( > )
	let isGreaterThan = a > b; // 50 > 65
	console.log(isGreaterThan);	

	// Less than ( < )
	let isLessThan = a < b; // 50 < 65
	console.log(isLessThan);

	/*GTE - Greater than or equal ( >= )
		with OR, it will return true if one condition is satisfied */
	let isGTorEqual = a >= b;
	console.log(isGTorEqual); // false

	//LTE Less than or equal ( <= )
	let isLTorEqual = a <= b;
	console.log(isLTorEqual); //true

	//Forced Coercion
	let num = 56;
	let numStr = "30";
	console.log(num > numStr); //true

	let str = "twenty";
	console.log(num > str); //false - since the string is not numeric, the string will not be converted to a number, or a.k.a NaN (not a number)

/* Logical AND Operator ( && )
	Requires all / both are true*/
	console.log("-----------------------------");
	let isLegalAge = true;
	let isRegistered = false;

	let allRequirements = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " + allRequirements); //false

/* Logical OR Operator ( || )
	Requires only one true*/
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Resul of logical OR Operator: " + someRequirementsMet); //true

/* Logical Not Operator ( ! )
Returns the oposite value*/
	someRequirementsMet = !isLegalAge;
	console.log("Result of logical Not Operator: " + someRequirementsMet);//false